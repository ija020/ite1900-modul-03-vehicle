package vehicle;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Formatter;
import java.io.IOException;
import java.util.Scanner;
import java.io.PrintWriter;

public class Car extends Vehicle {

  private int power;
  private Calendar productionDate;

  protected static final int DEFAULT_POWER = 24;

  public Car() {
    this(
      Vehicle.DEFAULT_NAME,
      Vehicle.DEFAULT_COLOUR,
      Vehicle.DEFAULT_PRICE,
      Vehicle.DEFAULT_MODEL,
      Vehicle.DEFAULT_SERIAL_NUMBER,
      Vehicle.DEFAULT_DIRECTION,
      DEFAULT_POWER);
  }

  public Car(
    String name,
    String colour,
    int price,
    int model,
    String serialNumber,
    int direction,
    int power)
  {
    super(name, colour, price, model, serialNumber, direction);
    this.power = power;

    // TODO: Figure out how this really should be done.
    this.productionDate = new GregorianCalendar(model, 0, 1);
  }

  public int getPower() { return power; }
  public void setPower(int power) { this.power = power; }

  public Calendar getProductionDate() {
    // .clone() to ensure that the caller can't modify the date
    // without calling .setProductionDate().
    return (Calendar)productionDate.clone();
  }

  public void setProductionDate(Calendar date) {
    // .clone() to ensure that the caller can't modify the date
    // without calling .setProductionDate().
    this.productionDate = (Calendar)date.clone();
  }

  @Override
  public void setAllFields() {
    super.setAllFields();
    int power = Common.readInt(getInput(), "Power: ");
    setPower(power);
  }

  @Override
  public void turnRight(int degrees) {

    // I interpret the requirement as between 0 and 360
    // exclusive.
    if(degrees <= 0 || degrees >= 360) {
      // TODO: Do something more sensible than fail silently
      return;
    }

    int direction = getDirection();
    direction += degrees;
    while(direction >= 360)
      direction -= 360;
    setDirection(direction);
  }

  @Override
  public void turnLeft(int degrees) {

    // I interpret the requirement as between 0 and 360
    // exclusive.
    if(degrees <= 0 || degrees >= 360) {
      // TODO: Do something more sensible than fail silently
      return;
    }

    int direction = getDirection();
    direction -= degrees;
    while(direction < 0)
      direction += 360;
    setDirection(direction);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(super.toString());
    Formatter out = new Formatter(sb);
    out.format("Power: %d\n", getPower());
    out.format("Production date: %1$tY-%1$tm-%1$td\n", getProductionDate());
    String str = out.toString();
    out.close();
    return str;
  }
  
  @Override
  public void accelerate(double factor) {
    double speed = getSpeed();
    if(speed >= 0 && speed < 0.01)
      speed = 0.5 * factor;
    else
      speed *= factor;
    if(speed > Driveable.MAX_SPEED_CAR)
      speed = Driveable.MAX_SPEED_CAR;
    setSpeed(speed);
    System.out.printf("%s accelerates to %.2f km/h.\n", getName(), getSpeed());
  }

  @Override
  public void breaks(double factor) {
    double speed = getSpeed();
    speed /= factor;
    setSpeed(speed);
    System.out.printf("%s slows down to %.2f km/h.\n", getName(), getSpeed());
  }

  @Override
  public void writeData(PrintWriter w) throws IOException {
    super.writeData(w);
    w.printf("%d,%s,", getPower(), Common.formatCalendar(getProductionDate()));
    if(w.checkError())
      throw new IOException();
  }

  @Override
  public void readData(Scanner in) throws IOException {
    super.readData(in);
    try {
      setPower(in.nextInt());
      setProductionDate(Common.parseCalendar(in.next()));
    } catch(Exception e) {
      throw new IOException("Exception occurred on parsing CSV values", e);
    }
  }
}

