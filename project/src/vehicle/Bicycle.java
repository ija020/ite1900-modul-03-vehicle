package vehicle;

import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Bicycle extends Vehicle {

  private int gears;
  private Calendar productionDate;

  protected static final int DEFAULT_GEARS = 3;

  public Bicycle() {
    this(
      Vehicle.DEFAULT_NAME,
      Vehicle.DEFAULT_COLOUR,
      Vehicle.DEFAULT_PRICE,
      Vehicle.DEFAULT_MODEL,
      Vehicle.DEFAULT_SERIAL_NUMBER,
      Vehicle.DEFAULT_DIRECTION,
      DEFAULT_GEARS);
  }

  public Bicycle(
    String name,
    String colour,
    int price,
    int model,
    String serialNumber,
    int direction,
    int gears)
  {
    super(name, colour, price, model, serialNumber, direction);
    this.gears = gears;

    // TODO: Figure out how this really should be done.
    this.productionDate = new GregorianCalendar(model, 0, 1);
  }

  public int getGears() { return gears; }
  public void setGears(int gears) { this.gears = gears; }

  public Calendar getProductionDate() {
    return (Calendar)productionDate.clone();
  }

  public void setProductionDate(Calendar productionDate) {
    this.productionDate = (Calendar)productionDate.clone();
  }

  @Override
  public void setAllFields() {
    super.setAllFields();
    int gears = Common.readInt(getInput(), "Number of gears: ");
    setGears(gears);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(super.toString());
    Formatter out = new Formatter(sb);
    out.format("Gears: %d\n", getGears());
    out.format("Production date: %1$tY-%1$tm-%1$td\n", getProductionDate());
    String str = out.toString();
    out.close();
    return str;
  }

  @Override
  public void turnRight(int degrees) {
    System.out.printf("The bicycle %s turns %d degrees to the right.\n", getName(), degrees);
  }

  @Override
  public void turnLeft(int degrees) {
    System.out.printf("The bicycle %s turns %d degrees to the left.\n", getName(), degrees);
  }

  @Override
  public void accelerate(double factor) {
    double speed = getSpeed();
    if(speed >= 0 && speed < 0.01)
      speed = 0.3 * factor;
    else
      speed *= 0.5 * factor;
    if(speed > Driveable.MAX_SPEED_BIKE)
      speed = Driveable.MAX_SPEED_BIKE;
    setSpeed(speed);
    System.out.printf("%s accelerates to: %.2f km/h.\n", getName(), getSpeed());
  }

  @Override
  public void breaks(double factor) {
    double speed = getSpeed();
    speed /= factor * 0.5;
    setSpeed(speed);
    System.out.printf("%s slowed down to %.2f km/h.\n", getName(), getSpeed());
  }

  @Override
  public void writeData(PrintWriter w) throws IOException {
    super.writeData(w);
    w.printf("%d,%s,", getGears(), Common.formatCalendar(getProductionDate()));
    if(w.checkError())
      throw new IOException();
  }

  @Override
  public void readData(Scanner in) throws IOException {
    super.readData(in);
    try {
      setGears(in.nextInt());
      setProductionDate(Common.parseCalendar(in.next()));
    } catch(Exception e) {
      throw new IOException("Exception occurred on parsing CSV values", e);
    }
  }
}

