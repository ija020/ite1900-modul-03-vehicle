package vehicle;

import java.util.Scanner;
import java.util.Formatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.io.PrintWriter;
import java.io.IOException;
import java.text.ParseException;

public abstract class Vehicle
  implements Comparable<Vehicle>, Cloneable, Driveable, Fileable {

  private String colour;
  private String name;
  private String serialNumber;
  private int model; // model year
  private int price;
  private int direction;
  private double speed;
  private Calendar buyingDate;

  private Scanner input;

  protected static final String DEFAULT_COLOUR = "Black";
  protected static final String DEFAULT_NAME = "Pelle";
  protected static final String DEFAULT_SERIAL_NUMBER = "YE 21809";
  protected static final int DEFAULT_MODEL = 1973;
  protected static final int DEFAULT_PRICE = 0x1C0FFEE;
  protected static final int DEFAULT_DIRECTION = 0;

  protected Vehicle() {
    this(
      DEFAULT_NAME,
      DEFAULT_COLOUR,
      DEFAULT_PRICE,
      DEFAULT_MODEL, 
      DEFAULT_SERIAL_NUMBER,
      DEFAULT_DIRECTION);
  }

  protected Vehicle(
    String name,
    String colour,
    int price,
    int model,
    String serialNumber,
    int direction)
  {
    // TODO: Check if parameters are legal values
    this.colour = colour;
    this.name = name;
    this.model = model;
    this.price = price;
    this.serialNumber = serialNumber;
    this.direction = direction;
    this.input = new Scanner(System.in);
    this.buyingDate = new GregorianCalendar();
  }

  public void setAllFields() {
    Scanner input = getInput();
    String name = Common.readString(input, "Name: ");
    String colour = Common.readString(input, "Colour: ");
    int price = Common.readInt(input, "Price: ");
    int model = Common.readInt(input, "Model: ");
    String serialNumber = Common.readString(input, "Serial #: ");
    int direction = 0;
    int speed = 0;

    setName(name);
    setColour(colour);
    setPrice(price);
    setModel(model);
    setSerialNumber(serialNumber);
    setDirection(direction);
    setSpeed(speed);
  }

  public abstract void turnLeft(int degrees);
  public abstract void turnRight(int degrees);

  public String toString() {
    Formatter out = new Formatter();
    out.format("Name: %s\n", getName());
    out.format("Serial #: %s\n", getSerialNumber());
    out.format("Model: %d\n", getModel());
    out.format("Price: %d\n", getPrice());
    out.format("Colour: %s\n", getColour());
    out.format("Speed: %.2f\n", getSpeed());
    out.format("Direction: %d\n", getDirection());
    out.format("Buying date: %1$tY-%1$tm-%1$td\n", getBuyingDate());
    String str = out.toString();
    out.close();
    return str;
  }

  public String getColour() { return colour; }
  public String getName() { return name; }
  public String getSerialNumber() { return serialNumber; }
  public int getModel() { return model; }
  public int getPrice() { return price; }
  public int getDirection() { return direction; }
  public double getSpeed() { return speed; }
  public Calendar getBuyingDate() { return (Calendar)buyingDate.clone(); }

  protected Scanner getInput() { return input; }

  public void setColour(String colour) {
    this.colour = colour;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public void setModel(int model) {
    this.model = model;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public void setDirection(int direction) {
    this.direction = direction;
  }

  public void setSpeed(double speed) {
    this.speed = speed;
  }

  public void setBuyingDate(Calendar buyingDate) {
    this.buyingDate = (Calendar)buyingDate.clone();
  }

  //public void setInput(Scanner input) {
  //  this.input = input;
  //}

  public int compareTo(Vehicle other) {
    return this.getPrice() - other.getPrice();
  }

  public Object clone() {
    Vehicle copy = null;
    try {
      copy = (Vehicle)super.clone();
    } catch(Exception e) {
      throw new AssertionError("Exception upon super.clone() call", e);
    }
    copy.buyingDate = (Calendar)this.buyingDate.clone();
    copy.input = new Scanner(System.in);
    return copy;
  }

  @Override
  public void stop() {
    setSpeed(0);
    System.out.printf("%s stops.\n", getName());
  }

  private static enum Field {
    NAME, COLOUR, PRICE, MODEL, SERIAL_NUMBER, DIRECTION, SPEED, BUYING_DATE
  }

  public void writeData(PrintWriter out) throws IOException {
    for(int i = 0; i < Field.values().length; i++) {
      out.print(getField(Field.values()[i]));
      out.print(",");
    }
    if(out.checkError())
      throw new IOException();
  }

  private String getField(Field field) {
    switch(field) {
      case NAME:
        return getName();
      case COLOUR:
        return getColour();
      case PRICE:
        return Integer.valueOf(getPrice()).toString();
      case MODEL:
        return Integer.valueOf(getModel()).toString();
      case SERIAL_NUMBER:
        return getSerialNumber();
      case DIRECTION:
        return Integer.valueOf(getDirection()).toString();
      case SPEED:
        return Double.valueOf(getSpeed()).toString();
      case BUYING_DATE:
        return Common.formatCalendar(getBuyingDate());
      default:
        throw new AssertionError("Unrecognized field " + field.toString());
    }
  }

  private void setField(Field field, String value) throws NumberFormatException, ParseException {
    switch(field) {
      case NAME:
        setName(value);
        break;
      case COLOUR:
        setColour(value);
        break;
      case PRICE:
        setPrice(Integer.parseInt(value));
        break;
      case MODEL:
        setModel(Integer.parseInt(value));
        break;
      case SERIAL_NUMBER:
        setSerialNumber(value);
        break;
      case DIRECTION:
        setDirection(Integer.parseInt(value));
        break;
      case SPEED:
        setSpeed(Double.parseDouble(value));
        break;
      case BUYING_DATE:
        setBuyingDate(Common.parseCalendar(value));
        break;
      default:
        throw new AssertionError("Unrecognized field " + field.toString());
    }
  }

  public void readData(Scanner in) throws IOException {
    in.useDelimiter(",");
    String[] csvValues = new String[Field.values().length];
    for(int i = 0; i < Field.values().length; i++)
      csvValues[i] = in.next();
    try {
      for(int i = 0; i < Field.values().length; i++) {
        setField(Field.values()[i], csvValues[i]);
      }
    } catch(Exception e) {
      throw new IOException("Exception occurred on parsing CSV values", e);
    }
  }
}

