package vehicle;

import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.List;
import java.util.ArrayList;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Date;

class Common {

  protected static String readString(Scanner input, String prompt) {
    System.out.print(prompt);
    String str = input.nextLine().trim();
    return str;
  }

  protected static int readInt(Scanner input, String prompt) {
    while(true) {
      try {
        System.out.print(prompt);
        int n = input.nextInt();
        input.nextLine();
        return n;
      } catch(InputMismatchException e) {
        System.out.println("The input must be a number!");
        input.nextLine();
        continue;
      }
    }
  }

  protected static String readChoice(Scanner input, String prompt, String... choices) {
    while(true) {
      System.out.print(prompt);
      String line = input.nextLine().trim().toLowerCase();
      for(String choice : choices)
        if(line.equals(choice.toLowerCase()))
          return choice;
      System.out.println("Invalid choice!");
    }
  }

  protected static List<Vehicle> getVehiclesByNameFromList(List<Vehicle> list, String name) {
    List<Vehicle> values = new ArrayList<>();
    for(Vehicle v : list)
      if(v.getName().equals(name))
        values.add(v);
    return values;
  }

  public static Calendar parseCalendar(String str) throws ParseException {
    Date date = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ROOT).parse(str);
    Calendar calendar = new GregorianCalendar();
    calendar.setTime(date);
    return calendar;
  }

  public static String formatCalendar(Calendar calendar) {
    DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ROOT);
    Date date = calendar.getTime();
    return df.format(date);
  }

}

