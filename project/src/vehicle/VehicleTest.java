package vehicle;

/**
 * TestVehicles oppretter Bicycle og Car objekter, legger disse i et ArrayList
 * Lar bruker manipulere data i arrayet på forskjellige måter
 */

import java.util.*;
import java.io.*;

public class VehicleTest {
  private boolean startupComplete = false;

  public static void main(String[] args) {
    VehicleTest vtest = new VehicleTest();
    try {
      vtest.menuLoop();
    } catch(IOException e) {
      System.out.println("IO Exception!");
      System.exit(1);
    } catch(CloneNotSupportedException e) {
      System.out.println("CloneNotSupportedException");
      System.exit(1);
    }
  }

  private void menuLoop() throws IOException, CloneNotSupportedException {
    Scanner scan = new Scanner(System.in);
    File file = new File("saveData");
    file.createNewFile();
    Scanner in = new Scanner(file);
    ArrayList<Vehicle> arr=new ArrayList<Vehicle>();
    Vehicle vehicle;

    if (!startupComplete) {
      in.useDelimiter(",");
      while(in.hasNext()) {
        String className = in.next();
        Vehicle veh = null;
        if (className.equals("Car")) {
          veh = new Car();
        }
        else if (className.equals("Bicycle")){
          veh = new Bicycle();
        }
        else {
          System.out.println("Invalid class in file:" + file.getName());
        }
        veh.readData(in);
        arr.add((Vehicle)veh.clone());
      }
      startupComplete = true;
    }
    if (arr.size() == 0) {
      //adding Test data, this check exists to prevent a duplicate of this data from being added every time the program is run.
      arr.add(new Car("Volvo","Black",85000,2010,"1010-11",163,0));
      arr.add(new Bicycle("Diamant","yellow",4000,1993,"BC100",10,0));
      arr.add(new Car("Ferrari Testarossa","red",1200000,1996,"A112",350,0));
      arr.add(new Bicycle("DBS","pink",5000,1994,"42",10,0));
    }
    while(true) {
      System.out.println("1...................................New car");
      System.out.println("2...............................New bicycle");
      System.out.println("3......................Find vehicle by name");
      System.out.println("4..............Show data about all vehicles");
      System.out.println("5.......Change direction of a given vehicle");
      System.out.println("6.........................Test clone method");
      System.out.println("7..................Test driveable interface");
      System.out.println("8..............................Exit program");
      System.out.println(".............................Your choice?");
      int choice = scan.nextInt();
      scan.nextLine();

      switch (choice) {
      case 1:
        {
          Car car = new Car();
          car.setAllFields();
          arr.add(car);
          break;
        }
      case 2:
        Bicycle bicycle = new Bicycle();
        bicycle.setAllFields();
        arr.add(bicycle);
        break;
      case 3:
        {
          // Printing information about all vehicles with the given
          // name is deliberate
          String name = Common.readString(scan, "Name of vehicle: ");
          List<Vehicle> list = Common.getVehiclesByNameFromList(arr, name);
          for(Vehicle v : arr)
            System.out.println(v);
          if(list.size() == 0)
            System.out.println("No vehicle by that name found.");
        }
        break;
      case 4:
        for(Vehicle v : arr)
          System.out.println(v);
        break;
      case 5:
        {
          String name = Common.readString(scan, "Name of vehicle: ");
          List<Vehicle> list = Common.getVehiclesByNameFromList(arr, name);
          if(list.size() == 0) {
            System.out.println("No vehicle by that name found.");
            break;
          }

          String leftRight = Common.readChoice(scan, "Direction [R/L]: ", "R", "L");
          int degrees = Common.readInt(scan, "Degrees [0-360]: ");

          for(Vehicle v : list) {
            switch(leftRight) {
            case "R":
              v.turnRight(degrees);
              break;
            case "L":
              v.turnLeft(degrees);
              break;
            }
          }
        }
        break;
      case 6:
        Car c1 = new Car();
        Car c2 = (Car)c1.clone();
        c2.setBuyingDate(new GregorianCalendar(1990,9,19));
        System.out.println(c1);
        System.out.println(c2);
        break;
      case 7:
        {
          Car car = new Car();
          car.setName("Car");
          car.accelerate(30.0);
          car.breaks(5.0);
          car.stop();
          Bicycle bike = new Bicycle();
          bike.setName("Bike");
          bike.accelerate(30.0);
          bike.breaks(5.0);
          bike.stop();
        }
        break;
      case 8:
        PrintWriter out = new PrintWriter(file);
        for (int i = 0; i < arr.size(); i++) {
          out.print(arr.get(i).getClass().getSimpleName() + ",");
          arr.get(i).writeData(out);
        }
        out.close();
        scan.close();
        System.exit(0);
      default:
        System.out.println("Wrong input!");
      }
    }
  }
}
